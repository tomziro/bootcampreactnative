console.log()
console.log("Tugas Conditional (PERMAINAN WAREWOLF)")
console.log("===============================================================")

//Program If-else
var nama = "Thomy";
var peran = "Penyihir";

if(nama=="" && peran==""){
    console.log("Nama harus diisi!")
}if(nama!=="" && peran==""){
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!")
}

if(nama==nama && peran=="Penyihir" ){
    console.log("Selamat Datang di Dunia Warewolf " + nama)
    console.log("Halo " + peran + " " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
}else if(nama==nama && peran=="Guard"){
    console.log("Selamat Datang di Dunia Warewolf " + nama)
    console.log("Halo " + peran + " " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
}else if(nama==nama && peran=="Warewolf"){
    console.log("Selamat Datang di Dunia Warewolf " + nama)
    console.log("Halo " + peran + " " + nama + ", Kamu akan memakan mangsa setiap malam!")
}else if(peran!=="Penyihir" && peran!=="Guard" && peran!=="Warewolf" && peran!==""){
    console.log("Selamat Datang di Dunia Warewolf " + nama)
    console.log("Halo " + nama + ", Peran Tersebut Tidak Ada Dalam Permainan. Silahkan Ketikkan Peran Lainnya (Penyihir, Guard, Warewolf)")
}
console.log("===============================================================")
console.log()
console.log()
console.log("Tugas Conditional (Switch Case)")
console.log("===============================================================")

//Program Switch Case
var tanggal;
var bulan;
var tahun;

tanggal = 16;
bulan = 2;
tahun = 2021;

if(tanggal>31){
    console.log("Format Tanggal 1 s/d 31")
}else if(tahun<1900){
    console.log("Format Tahun 1900 s/d 2000")
}

switch(bulan)
{
    case 1: {console.log(tanggal + " " +"Januari" + " " + tahun);break;}
    case 2: {console.log(tanggal + " " +"Februari" + " " + tahun);break;}
    case 3: {console.log(tanggal + " " +"Maret" + " " + tahun);break;}
    case 4: {console.log(tanggal + " " +"April" + " " + tahun);break;}
    case 5: {console.log(tanggal + " " +"Mei" + " " + tahun);break;}
    case 6: {console.log(tanggal + " " +"Juni" + " " + tahun);break;}
    case 7: {console.log(tanggal + " " +"Juli" + " " + tahun);break;}
    case 8: {console.log(tanggal + " " +"Agustus" + " " + tahun);break;}
    case 9: {console.log(tanggal + " " +"September" + " " + tahun);break;}
    case 10: {console.log(tanggal + " " +"Oktober" + " " + tahun);break;}
    case 11: {console.log(tanggal + " " +"November" + " " + tahun);break;}
    case 12: {console.log(tanggal + " " +"Desember" + " " + tahun);break;}
    default: {console.log("Format Bulan 1 s/d 12")}
}

console.log("===============================================================")