console.log()
console.log("1. Looping (While)")
console.log("===============================================================")

//Program While-Loop
var a = 0;
var text1 = "LOOPING PERTAMA"
var text2 = "LOOPING KEDUA"
var text3 = "I Love Coding"
var text4 = "I Will Become a Mobile Developer"

while(a<11)
{
    if(a==0)
        {
            console.log(text1)
        }
    else
        {
            console.log(a*2 + " " +  "-" + " " + text3);
        }
    a++;
}
while(a>0)
{
    if(a==11)
        {
            console.log(text2)
        }
    else
        {
            console.log(a*2 + " " +  "-" + " " + text4);
        }
    a--;
}

console.log()
console.log("2. Looping (For)")
console.log("===============================================================")

//Program For-Loop
var a = 0;
var text5 = "Santai"
var text6 = "Berkualitas"
var text7 = "I Love Coding"

for(a=1; a<21; a++)
{
    if(a%3 == 0 && a%2 == 1)
        {
            console.log(a + " " +  "-" + " " + text7)
        }
    else if(a%2 == 0)
        {
            console.log(a + " " +  "-" + " " + text6)
        }
    else
        {
            console.log(a + " " +  "-" + " " + text5)
        }
}

console.log()
console.log("3. Looping (Persegi Panjang)")
console.log("===============================================================")

//Program For-Loop-Condition1
var a = 0;
var hash = "#"

for(a=0; a<4; a++)
{
    var kosong="";
    for(b=0; b<8; b++)
    {
        kosong=kosong+hash
    }
    console.log(kosong)
}

console.log()
console.log("4. Looping (Tangga)")
console.log("===============================================================")

//Program For-Loop-Condition2
var a = 0;
var hash = "#"

for(a=0; a<8; a++)
{
    var kosong="";
    for(b=0; b<a; b++)
    {
        kosong=kosong+hash
    }
    console.log(kosong)
}

console.log()
console.log("5. Looping (Papan Catur)")
console.log("===============================================================")

// Program For-Loop-Condition3
var a = 0;
var hash = "#"
var kosong=" "

for(a=0; a<8; a++)
{
    var temp="";
    for(b=0; b<8; b++)
    {
        if((b-a)%2 == 0)
        {
            temp += kosong
        }
        else
        {
            temp += hash
        }
    }
    console.log(temp)
}
