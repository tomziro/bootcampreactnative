console.log()
console.log("1. (Range)")
console.log("===============================================================")

function range(startNum, finishNum)
{
    if(!finishNum)
    {
        return -1
    }

    var number = [];
    if(startNum > finishNum)
        {
            for (a = startNum; a >= finishNum; a--)
            {
                number.push(a);
            }return number;
        }
    for (a = startNum; a <= finishNum; a++)
        {
            number.push(a);
        }return number;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log()
console.log("2. (Range with Step)")
console.log("===============================================================")

function rangeWithStep(startNum, finishNum, step)
{
    if(!finishNum){
        return -1
    }
    var number = [];
    if(startNum > finishNum)
    {
        for (a = startNum; a >= finishNum; a -= step)
        {
            number.push(a);
        }return number;
    }
    for (a = startNum; a <= finishNum; a += step)
    {
        number.push(a);
    }return number;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log()
console.log("3. (Sum of Range)")
console.log("===============================================================")

function sum(angka_awal_deret, angka_akhir_deret, beda_jarak=1)
{
    if(!angka_awal_deret)
    {
        return 0
    }

    if(!angka_akhir_deret)
    {
        return 1
    }

    var number = [];
    if(angka_awal_deret > angka_akhir_deret)
    {
        for (a = angka_awal_deret; a >= angka_akhir_deret; a -= beda_jarak)
        {
            number.push(a);
        }return number.reduce((a, b) => (a + b), 0);
    }
    for (a = angka_awal_deret; a <= angka_akhir_deret; a += beda_jarak)
    {
        number.push(a);
    }return number.reduce((a, b) => (a + b), 0);
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0

console.log()
console.log("4. (Array Multidimensi)")
console.log("===============================================================")


var input = 
[
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]; 

function dataHandling(arr)
{
    arr.forEach(element => {
       console.log("Nomor ID", element[0]);
       console.log("Nama Lengkap", element[1]);
       console.log("TTL", element[2]);
       console.log("Hobi", element[3], "\n");
    });
}

dataHandling(input)


console.log()
console.log("5. (Balik Kata)")
console.log("===============================================================")

function balikKata(katakan)
{
    var hasil = ""
    for (a = katakan.length-1; a >= 0; a--) {
        hasil += katakan[a]
    }return hasil
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I

console.log()
console.log("6. (Metode Array)")
console.log("===============================================================")

var input2 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2(input2)
{
    input2.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    input2.splice(4, 4, "Pria", "SMA Internasional Metro");
    return input2
}

console.log(dataHandling2(input2));
var tgl = input2[3].split("/");
var bulan = 1;
tgl.sort(function(a, b){return b-a})
var date = input2[3].split("/").join("-");
var dislice = input2[1].slice(0, -9)

switch(bulan)
{
    case 1: {console.log("Mei");break;}
}

console.log(tgl)
console.log(date)
console.log(dislice)